package routes

import (
	"pyfb-apiban/src/app/controllers"

	"github.com/gofiber/fiber/v2"
)

// PrivateRoutes func for describe group of private routes.
func PrivateRoutes(a *fiber.App) {
	// Create routes group.
	route := a.Group("/api/v1")

	// Routes for sounds
	// Routes for GET method:
	route.Get("/bannedips", controllers.GetBannedIps)   // get list of all banned IPs
	route.Get("/bannedip/:ip", controllers.GetBannedIp) // check if an IP is banned
}
