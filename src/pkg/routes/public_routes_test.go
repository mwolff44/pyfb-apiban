package routes

import (
	"io/ioutil"
	"net/http/httptest"
	"testing"

	"github.com/ansrivas/fiberprometheus/v2"
	healthcheck "github.com/aschenmaker/fiber-health-check"
	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
)

func TestPublicRoutes(t *testing.T) {
	// Load .env.test file from the root folder
	if err := godotenv.Load("../../../.env.test"); err != nil {
		panic(err)
	}

	// Define a structure for specifying input and output data of a single test case.
	tests := []struct {
		description      string
		method           string
		route            string // input route
		expectedError    bool
		expectedCode     int
		expectedStatus   string
		expectedResponse string
	}{
		{
			description:      "get prometheus metrics",
			method:           "GET",
			route:            "/metrics",
			expectedError:    false,
			expectedCode:     200,
			expectedStatus:   "200 OK",
			expectedResponse: ``,
		},
		{
			description:      "get healthcheck",
			method:           "GET",
			route:            "/",
			expectedError:    false,
			expectedCode:     200,
			expectedStatus:   "200 OK",
			expectedResponse: `ok`,
		},
	}

	// Define Fiber app.
	app := fiber.New()

	// Define routes.
	app.Use(healthcheck.New())
	prometheus := fiberprometheus.New("pyfb-apiban")
	prometheus.RegisterAt(app, "/metrics")
	app.Use(prometheus.Middleware)
	PublicRoutes(app)

	// Iterate through test single test cases
	for _, test := range tests {
		// Create a new http request with the route from the test case.
		req := httptest.NewRequest(test.method, test.route, nil)
		req.Header.Set("Content-Type", "application/json")

		if test.description == "get healthcheck" {
			req.Header.Set("X-Health-Check", "1")
		}

		// Perform the request plain with the app.
		resp, err := app.Test(req, -1) // the -1 disables request latency

		// Verify, that no error occurred, that is not expected
		assert.Equalf(t, test.expectedError, err != nil, test.description)

		// As expected errors lead to broken responses,
		// the next test case needs to be processed.
		if test.expectedError {
			continue
		}

		// Verify, if the status code is as expected.
		assert.Equalf(t, test.expectedCode, resp.StatusCode, test.description)

		// Verify, if the status reason is as expected.
		assert.Equalf(t, test.expectedStatus, resp.Status, test.description)

		// Verify, that ouput is the expected response, that is OK
		if test.route == "/metrics" {
			assert.Equalf(t, test.expectedResponse, string(``), test.description)
		} else {
			body, err := ioutil.ReadAll(resp.Body)
			assert.Equal(t, nil, err, test.description)
			assert.Equalf(t, test.expectedResponse, string(body), test.description)
		}
	}
}
