package configs

import (
	"os"
	"strconv"
	"strings"
)

// ServerConfig represents the server configuration
// STAGE_STATUS="prod"
// SERVER_HOST="0.0.0.0"
// SERVER_PORT=8080
type ServerConfig struct {
	StageStatus       string
	ServerHost        string
	ServerPort        string
	ServerReadTimeout int
	ServerHeader      string
}

// DBConfig represents the database configuration
// DB_HOST="127.0.0.1"
// DB_PORT=5432
// DB_USER="pyfb-apiban"
// DB_PASSWORD="password"
// DB_NAME="pyfb-apiban"
// DB_SSL_MODE="disable"
// DB_MAX_CONNECTIONS=100
// DB_MAX_IDLE_CONNECTIONS=10
// DB_MAX_LIFETIME_CONNECTIONS=2
type DBConfig struct {
	DBHost                   string
	DBPort                   string
	DBUser                   string
	DBPassword               string
	DBName                   string
	DBSSLMode                string
	DBMaxConnections         int
	DBMaxIdleConnections     int
	DBMaxLifetimeConnections int
}

// RedisConfig represents Redis configuration
// REDIS_HOST="127.0.0.1"
// REDIS_PORT=6379
// REDIS_PASSWORD=""
// REDIS_DB_NUMBER=0
type RedisConfig struct {
	RedisHost     string
	RedisPort     string
	RedisPassword string
	RedisDBNumber int
}

// Config represents the applications config
/*
# JWT settings:
JWT_SECRET_KEY="secret"
JWT_SECRET_KEY_EXPIRE_MINUTES_COUNT=15
JWT_REFRESH_KEY="refresh"
JWT_REFRESH_KEY_EXPIRE_HOURS_COUNT=720
*/
type Config struct {
	Server         ServerConfig
	DB             DBConfig
	Redis          RedisConfig
	SoundDirectory string
}

// New returns a new Config struct
func New() *Config {
	return &Config{
		Server: ServerConfig{
			StageStatus:       getEnv("STAGE_STATUS", "prod"),
			ServerHost:        getEnv("SERVER_HOST", "0.0.0.0"),
			ServerPort:        getEnv("SERVER_PORT", "8080"),
			ServerReadTimeout: getEnvAsInt("SERVER_READ_TIMEOUT", 60),
			ServerHeader:      getEnv("SERVER_HEADER", "pfb-apiban"),
		},
		DB: DBConfig{
			DBHost:                   getEnv("DB_HOST", "127.0.0.1"),
			DBPort:                   getEnv("DB_PORT", "5432"),
			DBUser:                   getEnv("DB_USER", "pyfb-apiban"),
			DBPassword:               getEnv("DB_PASSWORD", "password"),
			DBName:                   getEnv("DB_NAME", "pyfb-apiban"),
			DBSSLMode:                getEnv("DB_SSL_MODE", "disable"),
			DBMaxConnections:         getEnvAsInt("DB_MAX_CONNECTIONS", 0),
			DBMaxIdleConnections:     getEnvAsInt("DB_MAX_IDLE_CONNECTIONS", 2),
			DBMaxLifetimeConnections: getEnvAsInt("DB_MAX_LIFETIME_CONNECTIONS", 0),
		},
		Redis: RedisConfig{
			RedisHost:     getEnv("REDIS_HOST", "127.0.0.1"),
			RedisPort:     getEnv("REDIS_PORT", "6379"),
			RedisPassword: getEnv("REDIS_PASSWORD", ""),
			RedisDBNumber: getEnvAsInt("REDIS_DB_NUMBER", 0),
		},
		SoundDirectory: getEnv("DIRECTORY", "pyfb-apiban"),
	}
}

// Simple helper function to read an environment or return a default value
func getEnv(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}

	return defaultVal
}

// Simple helper function to read an environment variable into integer or return a default value
func getEnvAsInt(name string, defaultVal int) int {
	valueStr := getEnv(name, "")
	if value, err := strconv.Atoi(valueStr); err == nil {
		return value
	}

	return defaultVal
}

// Helper to read an environment variable into a bool or return default value
func getEnvAsBool(name string, defaultVal bool) bool {
	valStr := getEnv(name, "")
	if val, err := strconv.ParseBool(valStr); err == nil {
		return val
	}

	return defaultVal
}

// Helper to read an environment variable into a string slice or return default value
func getEnvAsSlice(name string, defaultVal []string, sep string) []string {
	valStr := getEnv(name, "")

	if valStr == "" {
		return defaultVal
	}

	val := strings.Split(valStr, sep)

	return val
}
