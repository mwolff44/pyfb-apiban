package configs

import (
	"log"
	"testing"

	"github.com/joho/godotenv"

	"github.com/stretchr/testify/assert"
)

func TestNew(t *testing.T) {
	// Define a structure for specifying input and output data of a single test case.
	tests := []struct {
		description      string
		expectedResponse *Config
	}{
		{
			description: "Test .env.test",
			expectedResponse: &Config{
				Server: ServerConfig{
					StageStatus:       "prod",
					ServerHost:        "0.0.0.0",
					ServerPort:        "5000",
					ServerReadTimeout: 60,
					ServerHeader:      "pyfb_router",
				},
				DB: DBConfig{
					DBHost:                   "127.0.0.1",
					DBPort:                   "5432",
					DBUser:                   "pyfb-apiban",
					DBPassword:               "password",
					DBName:                   "pyfb-apiban",
					DBSSLMode:                "disable",
					DBMaxConnections:         100,
					DBMaxIdleConnections:     10,
					DBMaxLifetimeConnections: 2,
				},
				Redis: RedisConfig{
					RedisHost:     "127.0.0.1",
					RedisPort:     "6379",
					RedisPassword: "",
					RedisDBNumber: 0,
				},
				SoundDirectory: "sounds",
			},
		},
	}

	// Iterate through test single test cases
	for _, test := range tests {

		// Read env test file
		err := godotenv.Load("../../../.env.example")
		if err != nil {
			log.Fatal("Error loading .env.example file")
		}

		// Verify, that ouput is the expected response, that is OK
		output := New()
		assert.Equalf(t, test.expectedResponse, output, test.description)

	}
}
