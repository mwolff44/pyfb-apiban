package utils

import (
	"database/sql"
)

// NewNullString method for returning null value in sql query
func NewNullString(s string) sql.NullString {
	if len(s) == 0 {
		return sql.NullString{}
	}
	if s == "00000000-0000-0000-0000-000000000000" {
		return sql.NullString{}
	}
	return sql.NullString{
		String: s,
		Valid:  true,
	}
}
