package utils

import (
	"fmt"
	"pyfb-apiban/src/pkg/configs"
)

// ConnectionURLBuilder func for building URL connection.
func ConnectionURLBuilder(n string, c *configs.Config) (string, error) {
	// Define URL to connection.
	var url string

	// Switch given names.
	switch n {
	case "postgres":
		// URL for PostgreSQL connection.
		url = fmt.Sprintf(
			"host=%s port=%s user=%s password=%s dbname=%s sslmode=%s",
			c.DB.DBHost,     // os.Getenv("DB_HOST"),
			c.DB.DBPort,     // os.Getenv("DB_PORT"),
			c.DB.DBUser,     // os.Getenv("DB_USER"),
			c.DB.DBPassword, // os.Getenv("DB_PASSWORD"),
			c.DB.DBName,     // os.Getenv("DB_NAME"),
			c.DB.DBSSLMode,  // os.Getenv("DB_SSL_MODE"),
		)
	case "redis":
		// URL for Redis connection.
		url = fmt.Sprintf(
			"%s:%s",
			c.Redis.RedisHost, // os.Getenv("REDIS_HOST"),
			c.Redis.RedisPort, // os.Getenv("REDIS_PORT"),
		)
	case "fiber":
		// URL for Fiber connection.
		url = fmt.Sprintf(
			"%s:%s",
			c.Server.ServerHost, // os.Getenv("SERVER_HOST"),
			c.Server.ServerPort, // os.Getenv("SERVER_PORT"),
		)
	default:
		// Return error message.
		return "", fmt.Errorf("connection name '%v' is not supported", n)
	}

	// Return connection URL.
	return url, nil
}
