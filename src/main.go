package main

import (
	"pyfb-apiban/src/pkg/configs"
	"pyfb-apiban/src/pkg/middleware"
	"pyfb-apiban/src/pkg/routes"
	"pyfb-apiban/src/pkg/utils"
	"time"

	"github.com/ansrivas/fiberprometheus/v2"
	healthcheck "github.com/aschenmaker/fiber-health-check"
	"github.com/gofiber/fiber/v2"

	_ "pyfb-apiban/src/docs" // load API Docs files (Swagger)

	_ "github.com/joho/godotenv/autoload" // load .env file automatically
)

// @title pyfb-apiban API
// @version 1.0
// @description pyfb-apiban services API Docs.
// @termsOfService http://swagger.io/terms/

// @contact.name Mathias WOLFF
// @contact.url http://www.pyfreebilling.com
// @contact.email mathias@celea.org

// @license.name AGPL 3.0
// @license.url https://www.gnu.org/licenses/agpl-3.0.en.html

// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization

// @BasePath /api
func main() {
	// configuration

	// Define Application config.
	config := configs.New()

	// Define a new Fiber app.
	app := fiber.New(fiber.Config{
		// FiberConfig func for configuration Fiber app.
		// See: https://docs.gofiber.io/api/fiber#config
		ServerHeader: config.Server.ServerHeader,
		ReadTimeout:  time.Second * time.Duration(config.Server.ServerReadTimeout), // read timeout in seconds
	})

	// Custom Timer middleware
	app.Use(middleware.Timer())

	// Health check using header "X-Health-Check"
	app.Use(healthcheck.New())

	// Middlewares.
	middleware.FiberMiddleware(app) // Register Fiber's middleware for app.

	// Prometheus middleware
	prometheus := fiberprometheus.New("pyfb-apiban")
	prometheus.RegisterAt(app, "/metrics")
	app.Use(prometheus.Middleware)

	// Routes.
	routes.SwaggerRoute(app)  // Register a route for API Docs (Swagger).
	routes.PublicRoutes(app)  // Register a public routes for app.
	routes.PrivateRoutes(app) // Register a private routes for app.
	routes.NotFoundRoute(app) // Register route for 404 Error.

	// Start server (with or without graceful shutdown).
	if config.Server.StageStatus == "dev" {
		utils.StartServer(app, config)
	} else {
		utils.StartServerWithGracefulShutdown(app, config)
	}
}
