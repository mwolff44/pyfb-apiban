package controllers

import (
	"pyfb-apiban/src/platform/database"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

// GetBannedIps func gets all banned IPs.
// @Description Get all banned IPs.
// @Summary get all banned IPs
// @Tags pyfb-apiban
// @Accept json
// @Produce json
// @Success 200 {array} models.IPs
// @Router /v1/bannedips [get]
func GetBannedIps(c *fiber.Ctx) error {
	// Create database connection.
	db, err := database.OpenDBConnection()
	if err != nil {
		// Return status 500 and database connection error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Get all banned IPs.
	ips, err := db.GetIPs()
	if err != nil {
		// Return, if sounds not found.
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"error":  true,
			"msg":    "IPs were not found",
			"count":  0,
			"sounds": nil,
		})
	}

	// Return status 200 OK.
	return c.JSON(fiber.Map{
		"error": false,
		"msg":   nil,
		"count": len(ips),
		"ip":    ips,
	})
}

// GetBannedIp func checks if an IP is banned.
// @Description check if an IP is banned.
// @Summary check if an IP is banned
// @Tags pfb-apiban
// @Accept json
// @Produce json
// @Param ip path string true "IP address"
// @Success 200 {array} IP Address
// @Router /v1/bannedip/{ip} [get]
func GetBannedIp(c *fiber.Ctx) error {
	// Catch IP address from URL.
	ip, err := uuid.Parse(c.Params("ip"))
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Create database connection.
	db, err := database.OpenDBConnection()
	if err != nil {
		// Return status 500 and database connection error.
		return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{
			"error": true,
			"msg":   err.Error(),
		})
	}

	// Check if IP is banned.
	ips, err := db.GetIP(ip)
	if err != nil {
		// Return, if sounds not found.
		return c.Status(fiber.StatusNotFound).JSON(fiber.Map{
			"error":  true,
			"msg":    "IP is not found",
			"count":  0,
			"sounds": nil,
		})
	}

	// Return status 200 OK.
	return c.JSON(fiber.Map{
		"error": false,
		"msg":   nil,
		"ip":    ips,
	})
}
