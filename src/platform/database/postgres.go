package database

import (
	"fmt"
	"time"

	"pyfb-apiban/src/pkg/configs"
	"pyfb-apiban/src/pkg/utils"

	"github.com/jmoiron/sqlx"

	_ "github.com/jackc/pgx/v4/stdlib" // load pgx driver for PostgreSQL
)

// PostgreSQLConnection func for connection to PostgreSQL database.
func PostgreSQLConnection() (*sqlx.DB, error) {

	// Define database connection settings.
	config := configs.New()

	// Build PostgreSQL connection URL.
	postgresConnURL, err := utils.ConnectionURLBuilder("postgres", config)
	if err != nil {
		return nil, err
	}

	// Define database connection for PostgreSQL.
	db, err := sqlx.Connect("pgx", postgresConnURL)
	if err != nil {
		return nil, fmt.Errorf("error, not connected to database, %w", err)
	}

	// Set database connection settings:
	db.SetMaxOpenConns(config.DB.DBMaxConnections)                           // the default is 0 (unlimited)
	db.SetMaxIdleConns(config.DB.DBMaxIdleConnections)                       // defaultMaxIdleConns = 2
	db.SetConnMaxLifetime(time.Duration(config.DB.DBMaxLifetimeConnections)) // 0, connections are reused forever

	// Try to ping database.
	if err := db.Ping(); err != nil {
		defer db.Close() // close database connection
		return nil, fmt.Errorf("error, not sent ping to database, %w", err)
	}

	return db, nil
}
