package database

import "pyfb-apiban/src/app/queries"

// Queries struct for collect all app queries.
type Queries struct {
	*queries.SoundQueries          // load queries from Sound model
	*queries.SoundDirectoryQueries // load queries from Sound Directory model
}

// OpenDBConnection func for opening database connection.
func OpenDBConnection() (*Queries, error) {
	// Define a new PostgreSQL connection.
	db, err := PostgreSQLConnection()
	if err != nil {
		return nil, err
	}

	return &Queries{
		// Set queries from models:
		IpQueries: &queries.IpQueries{DB: db}, // from IP model
	}, nil
}
