module pyfb-apiban

go 1.16

require (
	github.com/ansrivas/fiberprometheus/v2 v2.1.2
	github.com/arsmn/fiber-swagger/v2 v2.15.0
	github.com/aschenmaker/fiber-health-check v0.1.1
	github.com/go-playground/validator/v10 v10.9.0
	github.com/gofiber/fiber/v2 v2.16.0
	github.com/google/uuid v1.0.0
	github.com/jackc/pgx/v4 v4.13.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/joho/godotenv v1.3.0
	github.com/stretchr/testify v1.7.0
)
